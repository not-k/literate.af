const perms = require('perms.js');
const http = require('http.js');
const path = require('path');
const lfs = require('litfs.js')('/d');

let router = require('express').Router();

router.get(/.*/, async function(req, res)
{   const relPath = req.url;
    // console.log('get /fs/'+relPath);
    if(!await perms.allowRead(relPath, req.session.user))
    {   return res.sendStatus(403);
    }
    // allow sandbox iframes to get files
    res.sendFile( lfs.abs(relPath),
        { headers: {'Access-Control-Allow-Origin': '*'} },
        err => err ? res.sendStatus(404) : 0 );
});

router.delete(/.*/, async function(req, res)
{   const relPath = req.url;
    console.log('delete /fs/'+relPath)
    if(!await perms.allowWrite(relPath, req.session.user))
    {   return res.sendStatus(403);
    }
    if(!await lfs.exists(relPath))
    {   return res.sendStatus(404);
    }
    await lfs.del(relPath);
    onUpdate(relPath, null);
    return res.sendStatus(200);
});

router.put(/.*/, async function(req, res)
{   const relPath = req.url;
    console.log('put /fs/'+relPath);
    if(!await perms.allowWrite(relPath, req.session.user))
    {   return res.sendStatus(403);
    }
    try
    {   var fields = await http.form(req);
        let filedata = fields.filedata[0];
        await lfs.write(relPath, filedata);
        onUpdate(relPath, filedata);
    }
    catch(e)
    {   console.log(e);
        return res.sendStatus(400);
    }
    return res.sendStatus(200);
});

exports.router = function() { return router; }

plugins = { 'file': saveFile };

exports.plugin = function(name, cb)
{   plugins[name] = cb;
}

async function onUpdate(relPath, docText)
{   if(!/.*\.lit/.test(relPath)) { return; }
    let cellSplit = String(docText).split(/^\/\/\//m);
    for(let cell of cellSplit) if(cell.match(/[^\s]/))
    {   cell = '///'+cell;
        let args = cell.match(/^\/\/\/.*/);
        cell = cell.substr(args[0].length+1);
        args = args[0].split(/\s+/).slice(1);
        let pname = args[0] ? args[0].match(/\^(\w+)/) : null;
        let plug = plugins[pname ? pname[1] : null];
        plug && await plug(relPath, args, cell);
    }
}

async function saveFile(base_path, args, cell)
{   if( args[1].match(/\.lit$/) || !args[1].match(/^[\w\-]+\.[\w\-]+$/) )
    {   return false;
    }
    let fname = path.dirname(base_path)+'/'+args[1];
    console.log('^file', fname);
    await lfs.write(fname, cell);
    return true;
}

#!/bin/bash

read -p 'set admin username: ' admin_name
while : ; do
    read -sp 'set admin password: ' admin_pass
    echo ''
    read -sp 'admin password (again): ' admin_pass2
    echo ''
    [[ ! -z $admin_pass && $admin_pass == $admin_pass2 ]] && break
    echo "password mismatch" ;
done

cookie_secret=$(head -c 20 /dev/urandom | base64)

NODE_PATH=server:$NODE_PATH \
    node --trace-uncaught \
    -e 'require("setup.js").setup()' \
    admin_name=$admin_name \
    admin_pass=$admin_pass \
    cookie_secret=$cookie_secret

const fs = require('fs');
const path = require('path');
const util = require('util');
const config = require('config.js');

const readFile = util.promisify(fs.readFile)
const writeFile = util.promisify(fs.writeFile)
const existFile = util.promisify(fs.exists)
const deleteFile = util.promisify(fs.unlink)
const moveFile = util.promisify(fs.rename)
const readDir = util.promisify(fs.readdir)
const mkdirp = util.promisify(require('mkdirp'));

// add restrictions to allowed paths
// e.g. leading ~ disallowed as universal "command" signal
const segment = /^[\w\-].*/

// create a semi-sandboxed view of the fs
module.exports = function(basePath)
{
    basePath = config.get('data_path')+basePath;
    function abs_path(relPath)
    {   let fullPath = basePath + relPath
        let segments = fullPath.split('/')
        if(!segments.every(t => t.length==0 || segment.test(t)))
        {   
            console.log(segments);
            return null;
        }
        return fullPath;
    }
    async function read(relPath)
    {   let fullPath = abs_path(relPath)
        if(!fullPath) { return null }
        return await readFile(fullPath)
    }
    async function write(relPath, data)
    {   let fullPath = abs_path(relPath)
        if(!fullPath) { return null }
        try { await mkdirp(path.dirname(fullPath)); } 
        catch(e) { /* fake exceptions */ }
        // TODO why is it doing this???
        // TODO mktemp
        await writeFile(fullPath+'.tmp', data)
        return await moveFile(fullPath+'.tmp', fullPath)
    }
    async function del(relPath)
    {   let fullPath = abs_path(relPath)
        if(!fullPath) { return  }
        return await deleteFile(fullPath)
    }
    async function exists(relPath)
    {   let fullPath = abs_path(relPath)
        if(!fullPath) { return false }
        return await existFile(fullPath)
    }
    async function ls(relPath)
    {   let fullPath = abs_path(relPath);
        // console.log('ls',fullPath);
        if(!fullPath) { return false; }
        try { return await readDir(fullPath); }
        catch(e) { return []; }
    }
    return {
        abs: abs_path,
        read: read,
        write: write,
        del: del,
        exists: exists,
        ls: ls,
    };
}
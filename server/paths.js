const litutil = require('litutil.js');

// top folders length 1 and 2 reserved
const specialTopName = '[a-z]{1,2}';
const topName = '[a-zA-Z][\\w\\-]{2,20}';
const name = '[a-zA-Z][\\w\\-]{0,20}';
const anyExt = '(\\.[\\w\\-]{1,10})+';
const litExt = '\\.lit';
const modExt = '(\\.min)?\\.js';

const docReg = new RegExp(`^/d/(${topName}/)?(${name}/){0,5}${name}${anyExt}$`);
const litReg = new RegExp(`^/d/(${topName}/)?(${name}/){0,5}${name}${litExt}$`);
const modReg = new RegExp(`^/m/(${topName}/)?(${name}/){0,5}${name}${modExt}$`);
const urlReg = new RegExp(`^/(${topName}/)?(${name}/){0,5}${name}${anyExt}$`);


const pathReg = new RegExp(`^/(${name})?(/${name}){0,5}$`);


exports.docPath = function(path)
{   if(path.match(docReg))
    {   return path;
    }
    if(path.match(modReg))
    {   return path.replace(/^\/m/, '/d').replace(/\.js$/, '.lit');
    }
    if(('/d' + path).match(docReg))
    {   return '/d' + path;
    }
    return null;
}
exports.docPathAbs = function(path)
{   let p = exports.docPath(path);
    return p ? litutil.arg('data_path') + p : null;
}
exports.modPath = function(path)
{   if(path.match(modReg))
    {   return path;
    }
    if(path.match(litReg))
    {   return path.replace(/^\/d/, '/m').replace(/\.lit$/, '.js');
    }
    if(('/m' + path).match(modReg))
    {   return '/m' + path;
    }
    // console.log('no modPath: '+path);
    // console.log('no modMatch: '+modReg);
    return null;
}
exports.modPathAbs = function(path)
{   let p = exports.modPath(path);
    return p ? litutil.arg('data_path') + p : null;
}
exports.reqName = function(path)
{   let modPath = exports.modPath(path);
    if(!modPath) { return modPath; }
    return modPath.match(/^\/m\/(.*)\.js$/)[1];
}
exports.folderPath = function(path)
{   if(path.match(pathReg))
    {   return path;
    }
    return null;
}
const path = require('path')
const util = require('util')

const crypto = require('crypto')

function isstr(s) { return (typeof s)=='string'||(s instanceof String); }
function safestr(s) { return isstr(s) ? s.replace(/[^0-9A-Za-z\-_]/g,"").substr(0,20) : ""; }
function toJson(o) { try { return JSON.stringify(o); } catch(err) { console.log('tojson::'+err+' '+o); return null; } }
function fromJson(o) { try { return JSON.parse(o); } catch(err) { console.log('fromjson::'+err+' '+o); return null; } }

function issafestr(s)
{ 
    var char = function(s) { return s.charCodeAt(0); }
    if(!isstr(s) || s.length > 32 || s.length == 0)
        return false;
    for(var i=0 ; i<s.length ; i++)
    {
        var c = s.charCodeAt(i);
        if(c >= char('A') && c <= char('Z'))
            continue;
        if(c >= char('a') && c <= char('z'))
            continue;
        if(c >= char('0') && c <= char('9'))
            continue;
        if(c == char('-') || c == char('_'))
            continue;
        return false;
    }
    return true;
}

exports.safestr = safestr;
exports.issafestr = issafestr;
exports.fromJson = fromJson;
exports.toJson = toJson;



const _mkdirp = util.promisify(require('mkdirp'));

exports.mkdirp = async function(path)
{   try 
    {   await _mkdirp(path);
    }
    catch(e) 
    {   // because some idiot thought 'mkdir -p'
        // throws an error if the dir already exists
        if(e.code != 'EEXIST')
        {   throw e;
        }
    }
}





var userRegex = '[a-zA-Z0-9\-_]{1,20}';
var docRegex = '[a-zA-Z][a-zA-Z0-9\-_]{0,20}(.[a-zA-Z0-9\-_]{1,10})?';
// limit to depth-5 intentional
var docPathRegex = new RegExp(`^/(${docRegex})?(/${docRegex})?(/${docRegex})?(/${docRegex})?(/${docRegex})?$`);

exports.segments = function(path)
{   let segments = [];
    let m = path.match(docPathRegex);
    for(let i=1 ; m && m[i] ; i++)
    {   segments.push(m[i]);
    }
    return segments;
}

exports.goodPath = function(path)
{   return !!path.match(docPathRegex);
}

var args = {};
for(let arg of process.argv)
{   const eq = arg.split('=');
    if(eq.length == 2 && eq[0].length && eq[1].length)
    {   args[eq[0]] = eq[1];
    }
    if(eq.length == 1 && eq[0].length)
    {   args[eq[0]] = true;
    }
}

exports.arg = function(str, fallback)
{   if(typeof args[str] === 'undefined') { return fallback; }
    return args[str];
}

exports.setArgs = function(data)
{   for(key in data) if(data.hasOwnProperty(key))
    {   args[key] = data[key];
    }
}

exports.randomString = async function(length)
{   return new Promise(function(resolve, reject) 
    {   crypto.randomBytes(length, function(err, buffer) 
        {   if(err) { return reject(err); }
            resolve(buffer.toString('base64').substr(0, length));
        });
    });
}

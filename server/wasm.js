const file = require('file.js');
const path = require('path');
const { exec } = require('child_process');

file.plugin('wasm', callWasm);

const emcc = 'emcc';
const cwd = require('config.js').get('data_path')+'/d/';
const defaultOpt = '-s WASM=1 -s BINARYEN_ASYNC_COMPILATION=0 -s SINGLE_FILE=1';

async function callWasm(base_path, args, cell)
{
    if(!args[1].match(/^[\w\-]+\.js$/)) { return false; }

    base_path = path.dirname(base_path.substr(1));

    let cmd = emcc+' '+defaultOpt+' -o '+base_path+'/'+args[1];
    cmd += ' -I'+cwd;

    let lines = cell.split('\n');
    for(line of lines)
    {   let parse = line.match(/^(\w+)\s+([\w\-\.\/]+)$/);
        if(!parse) { continue; }
        if(parse[1] === 'bind')
        {   cmd += ' --bind '+parse[2];
        }
        if(parse[1] === 'tail')
        {   cmd += ' --post-js '+parse[2];
        }
    }

    console.log('wasm exec', cmd);
    let res = await new Promise(cb => exec(cmd, {'cwd': cwd}, cb));
    console.log(res);
    return true;
}

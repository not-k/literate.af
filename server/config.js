const fs = require('fs');

const config_path = 'config/lit.json';

function fail(msg)
{   console.log('Config error');
    console.log(msg);
    console.log('Try "run setup"');
    throw msg;
}

var config = {}

try { config = JSON.parse(fs.readFileSync(config_path)); }
catch(e) { console.log('no existing config file'); }

for(let arg of process.argv)
{   let eq = arg.split('=');
    if(eq.length == 1) { eq.push(true); }
    config[eq[0]] = eq[1];
}

config['data_path'] = config['data_path'] || process.cwd()+'/public';

exports.get = function(key, fallback)
{   let v = config[key];
    if(v === undefined) { v = fallback; }
    if(v === undefined) { fail(key); }
    return v;
}

exports.path = config_path;

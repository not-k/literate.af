const data_path = require('config.js').get('data_path');

var router = require('express').Router();

router.get('favicon.ico', function(req, res)
{   res.sendFile(req.url, {root: data_path});
});

router.get(/.*/, function(req, res)
{   res.sendFile('d/notebook/main.html', {
      root: data_path,
      headers: {'Feature-Policy': 'midi *'}
    });
});

module.exports = router;

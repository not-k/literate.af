const util = require('util');
const path = require('path');
const perms = require('perms.js');
const lfs = require('litfs.js')('/d');

var router = require('express').Router();

router.get(/.*/, async function(req, res)
{
    try
    {   let reqpath = req.url;
        if(reqpath.match(/\.(\w)+$/)) { reqpath = path.dirname(reqpath); }
        let names = [];
        if(await perms.allowRead(reqpath, req.session.user))
        {   names = await lfs.ls(reqpath);
        }
        let items = [];
        if(reqpath != '/')
        {   items.push(path.dirname(reqpath));
            reqpath += '/';
        }
        for(let name of names) if(!name.match(/^\./))
        {   items.push(reqpath.substr(1) + name);
        }
        res.writeHead(200, {'Content-Type': 'application/json'});
        res.end(JSON.stringify({items: items}));
    }
    catch(e)
    {   console.log(e);
        return res.sendStatus(500);
    }
});

exports.router = () => router;

FROM emscripten/emsdk

WORKDIR /lit

COPY package*.json ./

RUN npm install

RUN chmod -R 555 /lit

EXPOSE 8080

ENTRYPOINT ["npm", "run"]

CMD ["start"]
